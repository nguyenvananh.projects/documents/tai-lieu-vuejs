
<h2 style="text-align: center">TÀI LIỆU TỔNG QUAN VỀ VUEJS</h2>

>Người thực hiện: **Nguyễn Văn Anh** ( *nguyenvananh.connect@gmail.com* )

---

![banner](https://gitlab.com/nguyenvananh.projects/documents/tai-lieu-vuejs/raw/master/medias/banner.png)

<h3>GIỚI THIỆU</h3>

>**Vue.js Là Gì ?**
Gọi tắt là Vue (phát âm là /vjuː/, giống như view trong tiếng Anh), Vue.js là một framework linh động (nguyên bản tiếng Anh: progressive – tiệm tiến) dùng để xây dựng giao diện người dùng (user interfaces). Khác với các framework nguyên khối (monolithic), Vue được thiết kế từ đầu theo hướng cho phép và khuyến khích việc phát triển ứng dụng theo từng bước. Khi phát triển lớp giao diện (view layer), người dùng chỉ cần dùng thư viện lõi (core library) của Vue, vốn rất dễ học và tích hợp với các thư viện hoặc dự án có sẵn. Cùng lúc đó, nếu kết hợp với những kĩ thuật hiện đại như SFC (single file components) và các thư viện hỗ trợ, Vue cũng đáp ứng được dễ dàng nhu cầu xây dựng những ứng dụng một trang (SPA - Single-Page Applications) với độ phức tạp cao hơn nhiều.
*Trích: https://vi.vuejs.org/v2/guide/index.html*

<h3>Structure a Vue.js Project</h3>

>Cấu trúc thư mục của một dự án Vue

---

Dưới đây là cấu trúc file root của Vue.js project
```
.
├── build
|
├── config
|
├── node_modules
|
├── *src
|
├── static
|
├── test
|
├── .badelrc
|
├── .editorconfig
|
├── .gitignore
|
├── .postcssrc.js
|
└── *index.html
|
├── package.json
|
└── README.md
```

Chúng ta sẽ chú ý tập trung vào folder `src`và tập tin `index.html` . Sau đây là một số mô tả về các thành phần còn lại: 
1. `build` : 
2. `config` :
3. `node_modules`: Thư viện cần dùng ( Cài đặt qua câu lệnh `npm install ` )
4. `static` : 
5. `test` : 
6. `package.json` : Khai báo các package cần cho project

Quay trở lại với  `src` và `index.html` 
1. `index.html` : Tập tin index của project ( chứa id app ). Tất cả các view đều được đổ vào id *root* này.
2. `src` : Tất cả code của dự án nằm trong thư mục src. Thư mục source của dự án

```
.
├── App.vue
├── assets
│   └── ...
├── components
│   └── ...
├── main.js
├── router
    └── index.js
```

Thông tin các thư mục: 
* **assets** --- Trong đây bạn để bất kỳ tài nguyên nào được nhập vào trong các components của bạn. ( Hình ảnh, video, fonts, css, ... )
* **components** --- Tất cả các components phụ dùng chung của project.
* **router** --- Tất cả các routes của các dự án của bạn (trong trường hợp của tôi có chúng trong index.js). Về cơ bản, trong Vue.js, tất cả mọi thứ là một thành phần. Nhưng không phải tất cả mọi thứ là một trang. Một trang có một route như "/dashboard", "/settings" hoặc "/search". Nếu một thành phần có một route đó định tuyến.

<h4>Quy ước đặt tên.</h4>
Dưới đây là một số quy ước đến từ văn bản chính thức của Vue.js mà bạn cần cấu trúc tốt dự án của mình:

* Tên component phải luôn luôn là nhiều từ, ngoại trừ gốc "App" components. Sử dụng "UserCard" hoặc "ProfileCard" thay vì "Card".
* Mỗi component nên có trong tập tin riêng của mình.
* Tên tệp của các components tệp đơn phải luôn là PascalCase hoặc luôn là kebab-case. Sử dụng “UserCard.vue” hoặc “user-card.vue”.
* Các components chỉ được sử dụng một lần trên mỗi trang nên bắt đầu bằng tiền tố "The", để biểu thị rằng chỉ có thể có một. Ví dụ: đối với navbar hoặc footer , bạn nên sử dụng TheNavbar.vue và hoặc TheFooter.vue.
* Luôn sử dụng tên đầy đủ thay vì viết tắt trong tên của các components của bạn. Ví dụ: không sử dụng "UDSettings", thay vì sử dụng "UserDashboardSettings".

<h3>Vòng đời của một Vue instance</h3>

---

![Vòng Đời](https://images.viblo.asia/e9438f46-f91b-4384-ab5e-2d64d866a15c.png) 

>Tham khảo tại video: https://www.youtube.com/watch?v=f8p_ull2jTE



<h3>Tạo Component trong Vue.js</h3>

>Quá trình tạo một Component trong Vue.js

---

Mỗi component sẽ nằm trong một file .vue và các component sẽ có tính lồng nhau. Component root ( Component cha) sẽ nằm ở file App.vue gán với id #app ở file `index.html`

**Bước 1:** Tạo file vue ứng cho component cần tạo. Ở đây ví dụ là component Header.
* Tạo file `HeaderComponent.vue` trong thư mục `components`

* Cấu trúc cơ bản của một file component: 
    ```html
    <template>
        <!-- Code HTML -->
    </template>

    <script>
    // Code Javascript
    export default {
        name: 'header-component',
        data()  {
            return {
                
            }
        }
    }
    </script>

    <style>
    /* Code Css */
    </style>
    ```
**Bước 2:** Khai báo và sử dụng component trong component khác: 
* Từ nơi cần sử dụng, `import` Component cần sử dụng. Trường hợp ví dụ này chúng ta sẽ dùng `HeaderComponent` trong `App` : 
    ```js
    import HeaderComponent from './components/HeaderComponent.vue';
    ```
* Đăng ký component `HeaderComponent` ở trong `App` . Code javascript sau khi  `import` và đăng ký  `components` trong `App.vue`
    ```js
    //import component
    import HeaderComponent from './components/HeaderComponent.vue';
    export default {
        name: 'App',
        //Khai báo thuộc tính components và đăng ký HeaderComponent : 
        components: {
            HeaderComponent
        }
    }
    ```

* Sử dụng component bằng tag HTML: Chỉ cần thêm tag `<HeaderComponent></HeaderComponent>` vào nơi cần sử dụng.

**Bước 3: Giao tiếp giữa các component**

![img](https://images.viblo.asia/da1269bf-51b7-4b88-8f68-3ed9cca24bef.png)

**Prop dữ liệu từ cha xuống con**
Mỗi component instance đều có một scope riêng của nó, nghĩa là mình không thể và cũng không nên trực tiếp gọi tới parent data trong child component template. Data có thể gửi xuống từ compoent cha thông qua một custom attribute là props. Ví dụ mình sẽ truyền data name từ component cha component-a xuống component-b. Sử dụng props trong child component (component-b.vue).

Trong đó component-a, component-b với nội dung lần lượt như sau:

```html
<!-- component-a -->
<template>
  <div>Component A</div>
  <component-b :name="name"></component-b>
</template>
<script>
export default {
  data() {
    return {
        name: 'component-a',
    }
  },
  methods: {
  }
}
</script>
```


```html
<!-- component-b -->
<template>
  // khi này component-b lấy được giá trị name của commpoent-a prop xuống
  <div>{{ name }}</div>
</template>
<script>
export default {
  prop: ['name'],
  data() {
    return {
    }
  },
  methods: {
  }
}
</script>
```

**Emit event từ con lên cha**

Trong component-b với nội dung như sau:
```html
<!-- component-b -->
<template>
  <button @click="selectComponentB"></button>
  <div>{{ name }}</div>
</template>
<script>
export default {
  prop: ['name'],
  data() {
    return {
    }
  },
  methods: {
      selectComponentB: function() {
          // phát ra sự kiện 'selectedComponentB' với data là 'component-b đã được chọn'
          this.$emit('selectedComponentB', 'component-b đã được chọn');
      }
  }
}
</script>
```
Khi đó component-a sẽ lắng nghe sự kiện mà component-b phát đi
```html
<!-- component-a -->
<template>
  <div>Component A</div>
  // lắng nghe sự kiện 'selectedComponentB' phát ra từ component-b và xử lý nó với function 'handleEvent' 
  <component-b :name="name" @selectedComponentB="handleEvent"></component-b>
</template>
<script>
export default {
  data() {
    return {
        name: 'component-a',
    }
  },
  methods: {
      handleEvent: function(data) {
          // in ra data từ component-b gửi lên
          console.log(data);
      }
  }
}
</script>
```